- [Comment organiser les feuilles de css](#Comment organiser les feuilles de css)

- [Faire la liaison de style css]( faire la liaison de style css)

----------------------------------------------------------------------------------------



# Comment organiser les feuilles de css

Pour bien organiser son projet ,on doit regrouper plusieurs feuilles de css 
en une seul feuille de css 

aller dans le dossier ``assets`` 

- créer un nouveau dossier qu'on appelle css
- glisser à l'interieur la feuille de styles.css 
- créer un ficher qu'on nomme ``main`` 


## Faire la liaison de style css

En haut de la page style.css on saisit L'URL suivant 

**@import url(main.css);**

A la suite de celà le fichier qui regroupera les imports ira chercher les proprietés
css dans le ficher d'origine.

le fichier d'origine doit porter une 'link' dans l'aqulle elle ira chercher 
des proprités.

> Exemple:
```
  < link rel="stylesheet" href="./assets/css/styles.css" >
```