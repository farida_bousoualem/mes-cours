# [les object](#les object)

* [Object destructuration]()
* [Object attribute access]()
* [ Object as string]()
* [Object keys and values]()

----------------------------------------------------



## Object destructuration:

> La distructuration d'un objet permet de récupérer 
>les attributs d'un objet afin d'y accéder directement pplus facilement 

* Pour céer un objet déstructurer , on crée une variable d'un objet 
vide qu'on assigne à un autre objet 

Exemple 

````javascript
onst user {
    firtsName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue',
}

const {} = user;

````
>Cela céer un objet déstructuré qui n'a pour l'instant aucun 
>attribut en relation avec l'objet initial

*Pour récupérer les attributs dont on a besoin ,on écrit:
````javascript
const user {
    firtsName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue',
}

const {age, lastName} = user;

console.log(age, lastName);
````
resultat:

34 "leProf"

## Object attribute access:

> ctte méthode set à recupérer l'attribut d'un objet quand on ne sait pas 
>quel est le nom de l'attributut à l'avance
>
>on l'utilise sur un site internet ou une application car l'utilisateur
>va faire desrecherches par couleurs de produits, etc.

* afin de recupérer des attributs que l'on ne connaît pas à l'avance, on crée un tableau
puis on parcourt ce meme tableau

Exemple

````javascript
const user {
    firtsName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue',
}

const attrs = ['eyes'];

attrs.forEach(attr => console.log(user[attr]));
````
>Attention!! l'attribut doit être renseigné dans le tableau sous forme 
>d'un string uniquemnt 

resultat

>bleu 

## Object as string:

>parfois au lieu de traiter les données sous forme d'un array
>on doit les traiter sous forme d'un objet 

* A la place d'avoir des index, nous avons alors des attributs clés
( dans notre exemple : hostel1,rooms)on a :

````javascript
const hostels = {
  hostel1: {
    id: 1,
    name: 'hotel rose',
    roomNumbers: 10,
    pool: true,
    rooms: {
      {
        roomName: 'suite de luxe',
        size: 2,
        id: 1,
      },
      {
        roomName: 'suite nuptiale',
        size: 2,
        id: 2,
      },
      {
        roomName: 'suite familiale',
        size: 4,
        id: 3,
      },
    },
  },
};

````
## Object keys and values:

>dans la librairie js il ya des commandes qui permettent de transformer
>les objets *keys* et *value* en *array*

* pour transfomer les keys des objets en tableau, il fait utiliser  la commande:
````javascript
const keys = Object.keys(hostels);

console.log(keys);
````
* on recupère alors les keys des objets sous forme de tableau

resultat

>["hotel1","hotel2","hotel3"]

* pour les values on utilise aussi la commande
````javascript
const values = Object.values(hostels);

console.log(values);
````
* on récupère alors les valeus des objets sous forme de tableau

resultat:

[{...},{...},{...},]

0:{id:1, name:"hotel rose",roomNumbers:10,pool:true,room:{...}}

1:{id:2, name:"hotel ocean",roomNumbers:15,pool:false,room:{...}}

2:{id:3, name:"hotel des pins",roomNumbers:7,pool:true,room:{...}}

* Pour les objets qui ont des objets imbriqués, on peut aussi les récupérer
````javascript
const rooms = Object.values(hostels.hotel1.rooms);

console.log(rooms);
````
au resultat on aua toutes les rooms de l'hotel1.

* Ces commandes Object.keys et Object.values permmettent d'utiliser toute 
la puissance des opérateurs

````javascript
const rooms = Object.values(hostels.hotel1.rooms);
console.log(rooms);

rooms.forEach((room) => console.log(room));
````
* On parcourt bien le tableau rooms grace à l'opérateur **forEach**.

