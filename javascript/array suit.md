- [reduce]()

- [ Find]()

- [Every]()

- [ some]()

--------------------------------------------------------------




## reduce
La methode réduce applique une fonction qu'est un [accumulateur(acc)]() et qui traite
chaque valeur d'une liste (de la gauchevers la droite) afin de la reduir
à une seule valeur.

a)**La fonction fléchée**
--------------------------
Il est possible d'utiliser la fonction fléchée que la fonction classic

Exemple 1 :
````
cnst tabNumber=[2,5,10,2,33]

cnst total=tabNumber .reduce((acc,valeurcourante)=>acc=acc+valeurcourante,0);

//on met 0 à la fin pour mantrer qu'on cherche des nombres 

console.log(total);
````
>resultat
>=52

exemple 2 :
````
On as const total=tabNumber

 const total=tabNumber

    .reduce((acc,valeurcourant)=>acc=acc+valeurcourent,'');
             
     consol.log(total)
             
// les string **' '** pour dire qu'on aura au resultat des lettres
````
>resultat
>abcd

## Find

C'est une methode qui renvoie le pemier  élément du tableau
 
 correspondant aux condition de la fonction.
 
 Exemple
 ````
    const hotelToFind=hostels.find(hotel=>hotel.name===hotel océan)
    
    console.log(hotelToFind);
````
>resultat
>
>hotel océan


## Every
Cette methode permet de tester si tous les élements correspond au condition imposeé 

par la fionction elle renvoie un booléen soit true ou fals
Il renvoie true si tout les éléments d'un tableau repond à un prédicat
il renvoie fals si il ya au moin un seul élement qui ne repond pas au prédicat

````
function estAssezGrand(element, index, array) {
  return element >= 10;
}
[12, 5, 8, 130, 44].every(estAssezGrand);   // false
[12, 54, 18, 130, 44].every(estAssezGrand); // true
````

## some

Cette methode permet de démantrer si il ya au moin un élement du tableau 

correspond aux conditions de la fonction donner elle renvoie true ou fals

Exemple
````
function test(element, index, array) {
  return element >= 10;
 const toto = [22, 7, 3, 180, 14].some(test);
console.log(toto); //true
````
