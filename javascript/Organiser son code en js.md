# [Organiser son code en js](#Organiser son code en js)
* [créer des fichiers séparès](#créer des fichiers séparès)

## créer des fichiers séparès:

----------------------------------
* Créer un fichier pour les données appeller _user.data.js_

* Créer un fichier la oû on peux utiliser les données(exo:plyground.js)

* Exporter les données du fichier _user.data.js_avec le mot clé export

Exemple

````javascript

const me = {
    firstName: 'farida',
    lastName: 'bouss',
    age: 35,
};

const user1 = {
    firstName: 'Myvon',
    lastName: 'Lachance',
    age: 30,
};

const user2 = {
    firstName: 'BOB',
    lastName: 'Bachelor',
    age: 50,
};

export const tab = [me, user1, user2];


````
* importer les données dans le fichier pluground.js

Exemple:

````javascript
import {tab} from './user.data';

console.log(tab);
console.tab(tab);
````