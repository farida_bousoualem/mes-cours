# [les fonctions](#les fonctions)

* [Déclarer une function](#déclarer une function)
* [Déclarer une arrow function](#Déclarer une arrow function)
* [Appeler les functions](#Appeler les functions)


## Déclarer une function:

pour déclarer une fonction il faut écrire le mot lés**function** suivie de nom qu'on souhaitelui donner
et des parenthèses
Exemple:

``
function testFunction();
``
* dans les parenthèses on rentre **les paramètres**
de la function 

Exemple:

````javascript
function testFunction(x, y){

};
````
* Entre les accolades on écrit les instructions de la fonction

Exemple:

````javascript
function testFunction(x, y){
   return x + y;
};
````

## Déclarer une arrow function:

>l'arrow function est une écriture abrégée des fonctions

* Pour declarer une arrow function il faut d'abord déclarer une **variable** puis integrer une **function**
dites **anonyme**ie on lui apas assigné de nom

exemple:

````javascript
const testFunction = (x, y)=> {return x + y};
````
>arrow function est essentiellement utilisée dans les fonctions de call back

* dans une fonction de call back( si on prend un exemple de *filter*), on a pas besoin de mettre des accolades,
on peut l'écrire de deux façons

Exemple:

````javascript
//1er methode

tab.filter((x, y)=> x + y);

//2em methodes

tab.filter((x, y)=> { 
    return x + y;
})
````

## Appeler les fonctions 

* Pour appeller les fonctions ou arrow function, il suffit d'écrire le nom de la fonction
on y ajout des valeurs aux paramètres:

Exemple:
````javascript
testFunction(2,3);
````


