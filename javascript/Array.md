# lES différentes méthodes  js
-----------------------------------------------------------------------------------
- [Arry length  ](#Arry length et)

- [forEch](#forEch)

- [map](#map)

- [filter](#filter)

- [sort](#sort)

- [posh](#posh)

- [concat](#concat)

- [splice](#splice)

- [slice](#slice) 

-------------------------------------------------------------------------

## Arry length:

La propriété length (longueur)  indique le nombre d'éléments présents dans le tableau.

 Elle est toujours supérieure au plus grand indice du tableau.
 
 ```javascript
const tab = ['me', 'user', 'usre1', 'user2'];

console.log(tab.length)

>resultat 
 4 elements
````
## forEch:

Elle permet de parcourir tout les éléments d'un tableau et d'exécuter une fonction
sur chaques éléments dans ce tableau
Exemple:


````javascript
const foods=['fish','bread','tuna']

foods.forEach((food)=>{
       console.log(food)     
});

````
>Resultat:
fish
dread
tuna

## map:

 map fonctionne de la même façon que forEach il va parcourir les éléments 
 de tableau un par un ,map permet également de crée un nouveau tableau 
  avec les resultats d'une fonction donnée
 
 et elle modifie letableau d'origine ainsi que le nouveau tableau
 or pour éviter cela on crée une variable **temp**cette variable fabrique un nouvel objet
 
Exemple:


````javascript
const tab2=tab.map((usr,index)=>{

     const temp={...user};

     temp.index=index

     return index;

console.table(tabl)
console.log(tab2)
});
````
 {...user} les trois points c'est pour dir à **temp** fabrique un nouvel objet

## filter:

La methode **filter**()crée et returne un nouveau tableau contenant 

tous les élement du tableau d'origine quinremplissent une condition déterminéé

par la fonction ``collback``

Exemple:

````javascript
const foods=['fish','bread','tuna']

const test=foods.filter(food =>food.length > 3);

  console.log(test)
````
>Resultats
>
>['fish','tuna']

## sort

Ilpermet de trier les élements de tableau dans ce même tableau,

et renvoie le tableau.

Exemple:

````javascript
const foods=['fish','bread','tuna']

foods.sort();

console.log(foods);

```` 
>Resultats

>['tuna','bread','fish']

## push

La méthode **push()** ajoute un ou plusieurs éléments à la fin 

d'un tableau et retourne la nouvelle taille du tableau.

Exemple:

````javascript
const foods=['tuna','bread','fish']

const count=foods.push(meat)

console.log(count)

console.log(foods)
````

>resultats

>console.log(count) nous donne 4
>et
>console.log(foods)
>
>['fish','bread','tuna','meat']

## concat

La methode **concat**combine tout les élements 

de tableaud'origine et renvoie la nouvelle combine formée,
ie elle permet de rajuter des élements

Exemple methode 1:

````javascript
const tab1=[1,2,3,4,];

const tab2=[5,6,7];

const tab3=[...tab1,...tab2];

console.table(tab3)

````
>Resultat
>
>[1,2,3,4,5,6,7]

dans ce cas les tois points c'est pour coupier un tableau

Methode 2:

````javascript
const tab1=[1,2,3,4,];

const tab2=[5,6,7,];

console.log=(tab2.concat('',tab1));
````
>Resultat
>
>[1,2,3,4,5,6,7]
et
 
si console.log=(tab1.concat('',tab2))
>Resultat
>[5,6,7,1,2,3,4]

## splice

Il sert a la fois pour supprimer et ajouter des éléments 

à même le tableau,il cée aussi un nouvel objet sans modifier celui qu'on a

EXEMPLE :

````javascript
const days=[lundi,mercredi,jeudi];
      
      days.splice(1,0,mardi);
//la il va inserer le resultat à l'index 1

console.log(days);
//il va ajouter un élément au tableau
````

>Resultat
>
>['lundi','mardi','mercredi','jeudi']

## slice

Il renvoie un objet de tableau definiepar un indice de début

et un indice de fin  sachant que la fin n'est pas inclus

,le tableau d'origine ne sera pas modifier

Exemple:

````javascript
const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

console.log(animals.slice(2));
// resultat ["camel", "duck", "elephant"]

console.log(animals.slice(2, 4));
// resultat ["camel", "duck"]

console.log(animals.slice(1, 5));
// resultat ["bison", "camel", "duck", "elephant"]
````








