# [la récursivité d'une fonction](#)

> La recursivité d'une fonction c'est quand on appelle une fonctionà l'interieur
d'une cette même fonction 

* Quand on appelle une fonction de manière récursive (l'appeller à l'interieur d'elle même),
on fonction de cette function elledécide de se erminer oude continuer (elle s'auto appelle à nouveau).

Par exemple générer un nombre aléatoire unique (il ne doit jamais y avoir duex fois le meme nombre)

````javascript
//on crée une constente avec un tableau 
const allReadyUsedNumbers = [];

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1 ) + min);
}
//on crée la function unique (Uid) ie ce qu'on veux ecrir une suelfois 
 function generateUid(min, max) {
// ongénère une valeure 
    const temp = randomIntFromInterval(min, max);
    if (allReadyUsedNumbers.length === max) {
        return;
    }
// on utilise find afin de parcourir toutes les valeures de tableau
    if (allReadyUsedNumbers.find((value)  => value === temp)) {
        return generateUid(min, max);
    } else {
//a chaque qu'on a générer une valeure il faut la pusher dans le tableau [];
        allReadyUsedNumbers.push(temp);
//on ditsi temp existe déja dans le tableau on va return temp
        return temp;
    }
}

console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(generateUid(1, 10));
console.log(allReadyUsedNumbers);

````
* Le resultat est bien un nombre aléatoire qui se génere et si il se génére une 

éem fois ,on régénere un nouveau nombre aléatoire et si on a utilisé tous les 
 nombres disponibles, on aura indefined donc on sort de la function 
 
resultat:
>7
>5
>1
>6
>3
>2
>10
>4
>undefined

*Atention!!! il ne faut pas oublier de mettre desconditions de 
sortie afin de ne pas avoire de boucle infinie et donc d'avoir un message d'erreur 
stack overflow*

