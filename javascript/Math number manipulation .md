# Numbers manipulation

 - [ Notes et exemple](#Notes et exemple)
 
 - [Quelques outils apartenand à librairie Math](#quelques outils appartenand a la librairie Math )
 
 - [Outils n'appartenant pas a la librairie Math](#Outils n'appartenant pas a la librairie  Math)
-----------------------------------------------------------------------------------------




## Notes et exemple:
A noter que j.s ne donne pas des bon résultat si l'opération décimale 

Exemple1: décimal
      
      console.log(0.1 + 0.2)
      
      Résultat:0.30000000000004 
Exemple2: entier 
       
       cosole.log(1 + 2)
       
       Résultat:3
    
Pour cela on utilise les outils de la [librairie Math]()

## Quelques outils apartenand à librairie Math:

### [floor]() renvoie le plus grand entier qui est inférieur ou égal à un nombre
 
Exemple
           
         console.log(math.floor(10.9))
         
       Résultat: 10
### [Round]() retourne la valeur d'un nombre arrondi à l'entier le plus proche.  
 
 Exemple:
 
        console.log(Math.round(10.5));
        
      Resultat 11
      
        console.log(math.round(10.3));
        
      Resultat 10
      
## Outils n'appartenant pas a la librairie Math

### [tofixed]() il permet de prendre les deux ou trois chiffres après la virgule

Exemple

         console.log(10.6666668787.toFixed());
         
       Résultat 10.66666668787 car toFixed prend les nombres comme des stringues 
        
pour corriger ces problèmes on utilise (parsint ou bien parsfloat)

### [parsint]() analyse et convertit une chaine de caractères, fournie en argument, en un entier dans la base souhaitée.

### [parsfloat]() permet de transformer le stringue en nombre afin de fair l'opération normalement 

Exemple 
      
      console.log(parsfloat.('10.67') + parsint('10')
      
    Resultat 20.67

Exemple 

      console.log(10.6777.tofixed(2) + 10);
      
      
    Resultat 10.677710 car tofixed le prend comme un stringue 
    
    
 Exemple
 
     console.log(parsfloat(10.6777.tofixed(2) + 10);
     
    Resultat:20.68   car parsfloat à transformer le stringue en nombre.
    
 --------------------------------------------------------------------------------
 
## les Otils Mathématiques:

>il existe une librairie mathématique dans javascript qui s'appelle Math

### [Math.round]()
> Math.round est un outil qui arrondi un nombre 

* Math.round arrandi à la valeur la plus proche du nombre entier
Exemple:
pour 10,4 resultat sera **10**
pour 10,8 resultat sera **11**, on l'écrit 
        
        console.log(Math.round(10.4));
        
### [Math.floor]()
 
 >Math.floor est un outil qui supprime les chifres aprés la virgule

* Math.floor supprime les chifres aprés la virgule pour ne laisser q'un nombre entier,on l'écrit:
        
        console.log(Math.floor(10.8));
resultat est 10

### [ toFixed ]() 
>toFixed est un outil qui n'appartient pas à la librairie Math
>il permet de couper les chifres aprés la virgule à unelongueur voulue

exemple :
si on souhaite de garder que 2 chiffres aprés
 la virgule(pour payer en euros par exemple), on l'écrit comme telle 
 
        console.log(10.6666678787856.toFixed(2));
        
le resultat est  10,67

> Anoter ,enplus de couper le nombre en nombre de virgule souhaitées,
>toFixed arrondi le dernier chifre après la virgule

* Attention!! toFixed ne renvois pas un nombre mais il renvoit un string
Exemple:

        console.log(10.6666678787856.toFixed(3) +10);
        
le resultat n'est l'addition de 10.667 + 10 mais la concaténation de '10.667' + '10'

le resultat est 

10.66710

### [parseInt et parseFloat]()

>Elle permettent de transformer un string en nombre

* ParseInt est utiliser pour les nombres entier et parseFloat pour les nombres décimaux

Exemple

````javascript
console.log(parseInt('10') + 10);

console.log(parseFloat('10.67') + 10);
````
le resultat est 20
et 20,67

### [Math.random]()

> Math.random permet de crée un nombre aléatoire

* Il créeun nombre aléatoire comprie entre 0 et 1
le resultat n'arrive jamais à 1comme si il est exclu 

Exemple  

        console.log(Math.random());
        
resultat est un nombre aléatoire  entre 0 et 1 du type 0.651458511563

>Anoter ,en informatique l'aléatoire n'eexiste pas c'est un nombre pseudo aléatoire
> qui dépond du processuce et de la mémoire vive 

* Pour obtenir un nombre plus grand on écrit

        console.log(Math.random()* 1000 + 1);
        
resultat
 
 859.196424767064
 
* Pour obtenir, un nombre entier ,on combine avec Math.floor
Exemple

        console.log(Math.Floor(Math.random()* 1000 + 1;
        
 resultat
        
>324

>Astuce: on ajoute 1 pour ne pas avoir 0

* Pour crées un nombre aléatoir comprie entre 2 nombres, il faut alors créer une fonction

Exemple

````javascript
function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min) + 1);
}

console.log(createRandomNumber(10, 15));
```` 
resultat

onobtient des nombres aléatoir entre entre 10 et 15

12
13
14 ect
         
      
        