# -   [JAVASCRIPT](#JAVASCRIPT)

   -[Definition](#definition)

   -[Comment lier une page html avec le code javascript](#Comment lier une page html avec le code javascript)
   
   -[Types des valeurs primitive](#Types des valeurs primitive)
   
   -[Les Variables](#Les variables)
   
   -[Les Fonctions](#Les Fonctions)
   
   -[Remarque](#Remarque)
   
   
   
## definition 

Le javascript est un langage de script
qu'est utiliser sur le web ,c'est aussi 
un langage d'animation ,il permet de 
dynamiser notre site.

En javascriptun texte se présente sous forme de petit guillemets qu'on appelle 
 des ['singl cote']() ou bien des[ "doubles cote"]() 
 et on donne un ordre pour la fin qui sereprésente en [**;**]()
 
 ## Comment lier une page html avec le code javascript
 
Sur la page html, dans la balise [< head > < / head> ]()  <head></head> 

on ecrit la balise suivante afin de pouvoire  donner l'ordre a notre javascript


>  < script > src="index.js"< / script >


 ## Types des valeurs primitive
 
 Chaque valeur qu'on va pouvoir créer et manipuler en javascript
  va obligatoirement appartenir à l'un de ces types de valeur 
  il en existe 6 valeurs primitive
  
 
 1- **string ou chaine de caractaires en (français)**
 
 2-  **number  ou nombre en (français)**
 
 3- **booléan ou booléen en (français)**
 
 4- **null   ou  vide   en  (français)**
 
 5- **undefined ou indéfini en  (français)**
 
 6- **NaN ou n'est pas un nombre en  (français)**
 
 
 > Exemple  

>(**'**je suis developpeur en javascript et....**'**)**;**


 **premièrement** : lordinateur va reconnaitre le début et la fin de votre texte par le point virgule
 
 **deuxiemement** : lordinateur analysera le type de donnée :si elle sont des variable ou bien des chifres.
 
  C'est là que les guillemets vont rentrer en jeu 
 
 "doble cotes " et 'simple cotes'
 
 >Exemple

On sais que lesguillemets servent à délimiter la chaine :

'je m'appelle java' : calà ne fonctionera pas car l'ordinateur prend l'apostrophe
 
comme un délimiteurs , pour celà on utilise un antislash **alt gr + 8**
> exemple : 'je m'\appelle java'

## Les Variables

Se sont des cases dans lesquelle on stock des valeurs pour faire fonctionner une varianle

> exemple 

 var name = 'java';

  consol . log (name)

de même pour les les number

var number1 = 2;
var number2 = 5;

    consol . log (number1 + number2);
    
on as deux autres variable [const]() et [let]() conrairement au mot clé  [var]() 

le mot clé const accepte qu'une suel fois le [=]()

## Les Fonctions

Une fonction est destiné à exécuter une tâche bien spécifique
et qu'on poura si besoin utiliser à plusieurs reprise .


 ``const number1 = 2``;
 ``const number2 = 5``;
 ```
 [fonction]() addition  (x,y) {
  
 consol . log ;(x + y)
}
 
[fonction]() sustraction  (x,y) {
  
 consol . log (x - y)
  ```
## Remarque

* On note que le mot clé const on l'assigne que à une suel et unique fois 

contrairement au mot clé let qu'on peut assigner plusieur fois 

*une fonction peut prendre plusieurs paramttres (variable) même si c'est const .








