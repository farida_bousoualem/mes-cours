# [La boucle for](#la boucle for)
* [Générer un tableau]()


## Générer un tableau:

> La boucle for permet de génerer des tableaux

* Elle va répéter une ou plusieurs actions, un nombre de fois
déterminer par les paramèttres que l'on assigne à la boucle

> On a 3 paramètres:

>le 1er qui s'appelle l'initialisateur et dans lequel on indique  généralement
>qu'on initailise la bocle àl'index 0

>Le 2em est le condition de continuation, tant que la condition n'est pas remplie,onreste dans la boucle

>Le 3em est le paramètre qui indique ce que l'on répete à chaque tour de boucle en général on indique  que 
>on passe de l'index 0 à l'index 1 et ainsi de suit, que l'on écrit 
>i = i + 1       

Exemple si on veux répéter 20 fois coucou on écrit:
````javascript
for (let i = 0; i < 20; i++) {
    console.log('coucou');
}
````
**Attention aux conditions de sorties d'une boucle afin de ne pas créer une boucle infinie!**
