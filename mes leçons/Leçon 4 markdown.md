   - [MARKDOWN](#Markdown)
   
   -[Les principaux usage de la syntax Markdown](#Les principaux usage de la syntax Markdown)
   
   -[Des liens hypertexte ](#Des liens hypertexte)
   
   
   
   
# MARKDOWN

   Est un langage de code qui permet de prendre des notes d'une manière éfficace
   
   
## Les principaux usage de la syntax Markdown

  * pour souligner un titre  utiliser le signe Dièse **#**
   
   - **# titre 1**
   - **## titre 2**
   - **### titre 3**
   
  ------------------------------
   * Mot en caractaire gras:
  
    
   ** Double asterisque: **  **double astérisque**
   
   * mot en étalique * :      
   
   * un simple asterisque * :   *simple astérisque*
  
   -----------------------------
* pour créer une liste on met un espace entre l'asterisque et le texte qui suit  

* pour une citation :il faut ajouter ce symbole devant  **>**


## Des liens hypertexte

* pour créer un lien Markdown on utilise les crochetset les parenthèses dans 
lordre indiquée

>Exemple
[ ]
et ( ) 

[un exemple de lien](https://www.bing.com" markdown ")
 
qu'on peux consulter 

``: cest pour faire des petites sitations (voire exemple 1 plus bas)

 ``': c'est pour faire des grandes sitations (voire exemple 2 plus bas )


>exemple 1 : 
``exemple de teste n° 1``


> exemple 2: 

```
 exemple 
h1{
 background:red;
}
```

 
  
   
   
  

 
