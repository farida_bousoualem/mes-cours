- [Comment créer une page web](#Comment créer une page web)

    - [Information sur la page html](#Information sur la page html)
    
     - [Quelques explications des balises html](#Quelques explications des balises html )
     

# Comment créer une page web

- crée un dossier que vous appeliez par exemple *mon premier dossier*
- Ensuite créer un fichier que vous appeliez *index.html*

## Information sur la page html

La structure de la page **html** est toujours la même 

ie: en tête de la page on a [< !doctype html>](!doctype  ) cela veux dir qu'on utilise la verssion **html5**

en suite on a la balise  [< html>](html) :élement racine ,il entoure toute la page c'est à cette endroit qu'on peux mettr 
l'attribut (la langue souhaitée)

On a aussi la balise [< head>](#head) :elle determine des information sur la page 
ie:
- Le titre de la page web
- l'encodage...ect

on a la balise [< body>](#body)  : qui contient le contenu 

la balise [< meta>](#meta) indique l'encodage des caractaires qui est en [utf8](utf8)

la balise [< titel>](#titet) c'est là qu'on ecrit le titre de la page web
## Quelques explications des balises html

Dans la balise [< body>](#body) on écrit h1 + tabulation on aura
 
 une balise fermante et une balise ouvrente [< h1>](h1)[< / h1 >](#h1) elle contient le titre de la page 
 
 exemple  [< h1>](#h1)*mon super titre de ma page web* [< / h1>](#h1)

[< p>](#p) [< / p>](#p) une  balises pour écrire le paragraphe

[< div>](#div)[< /div>](div) c'est une sous division de la page 

``ie = cela veut dire``
