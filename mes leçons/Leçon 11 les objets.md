-[Les objets](#Les objets)

-----------------------------------------

# Les objets

En javacsript,un objet est une entité à part entiere qui possède

des proprietés, ces propriétés poron etre :la couleur ,la forme,le poids....ect

>Exemple:

```
  const frst Name = 'lola';
  const last Name = 'solo';
  const age = 20 ;
  const adresse = 6 rue de la joie ;

on va donc remplacer toutes ces "const"avec une entité objet "const you = {};"

const you = {
       frst Name : 'lola',
       last Name : 'solo',
       age : 20 ,
       adresse : 6 rue de la joie ,
};
console.log(you.fist Name);
console.log(you.last Name);
console.log(you.age);
console.log(you.adresse);

```

