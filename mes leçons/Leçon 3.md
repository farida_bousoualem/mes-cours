- [Qu'est ce que CSS ](#Qu'est ce que CSS)

    -[A quoi sert le CSS](#A quoi sert le CSS)
    
    -[Quelques informations de style](#Quelques information de style)
    
    -[Exemple de règle de style ](#Exemple de règle de style)
    
    -[Quelques poprietés raccourcies](#Quelques poprietés raccourcies)
    
    
    
    
    
# Qu'est ce que CSS 

De Langlais **``Cascading styl sheets``** forme un langage informatique qui

 decrit la présentation des documents **html**


## A quoi sert le CSS

Elle sert à mettre de la mise en pabge et donner de dynamismes

 à notre html.

## Quelques informations de style

Lorsqu'on place une class dans le styl css elle doit  être toujours 

 précéder d'un point 

si cela n'est pas une classe elle s'ecrira normalement dans le styl css

## Exemple de règle de style

````javascript
.content{
    background:red
}

h1{   
    text color:gris
}
````

# Quelques poprietés raccourcies

 - Background: est une proprieté raccourcie permet de difinir les
 
    differentes valeurs des proprietés
    
     liées à la gestion des arrieres plans d'un elément (couleur,image,origine,talle ..).
       
       
  - Border-width: est un raccourcie qui difinie la bordure d'un elément .  
  
  - Txt-aling: est un raccourcie qui difinie la linge de texte
  
  