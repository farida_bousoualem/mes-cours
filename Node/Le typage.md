# [Le typage]()

* [ typer Les élements en typescript](#)
* [la valeur retour d'une fonction]()


## Le typage:

Le typage est un langage quiq consiste à preciser en quel type sont 
les objets 
* string
* number
* entier...ect

## typer Les élements en typescript:

* Avant tout il faut toujours commancer à cée un dossier  model**s** avec **s**

* puis on crée un fichier interface pour chaque objet 
par exemple si on à des objets de type *company et users*:

comme suit :

````javascript
 const company = {
    name: '',
    address: '',
    size: '',
    users: [],
}

let users = [
    {
        id: 1,
        lastName: 'toto',
        age: 34,
    },
    {
        id: 2,
        lastName: 'toto1',
        age: 35,
    },
    {
        id: 3,
        lastName: 'toto2',
        age: 36,
    }
]
````   
on va créer un fichier **company.model.ts**
pour les Objets *company* et un autre fichier **user.model.ts**
pour les *user*
à savoir lorsqu'on crée des fichiers  on ne met jamais  de **s** pour nomer ce fichier 

* Dans chaque fichier il faut exporter l'interface 

comme suite:

Il faut comancer à exporter  par les plus profond de la liste 
comme ici c'est user on export d'abord **UserModel**

````javascript
export interface UserModel {
    id: number;
    lastName: string;
    age: number;
}
````
puis 
````javascript
export interface CompanyModel {
    name: string;
    address: string;
    size: number;
    users: UserModel[];
}
````
une fois qu'on à exporter ie typer les fichiers typecript

pour commencer à travailler on ajoute deux points puis le type de chaque 
objets
comme suite:

````javascript
import {CompanyModel} from './company.model';
import {UserModel} from './user.model';

const company: CompanyModel = {
    name: '',
    address: '',
    size: '',
    users: [],
}

let users: UserModel[] = [
    {
        id: 1,
        lastName: 'toto',
        age: 34,
    },
    {
        id: 2,
        lastName: 'toto1',
        age: 35,
    },
    {
        id: 3,
        lastName: 'toto2',
        age: 36,
    }
]
````
## la valeur retour d'une fonction:
````javascript
// on fait une fonction avec un id par exemple 
//on sais que l'id et de type number d'apres **export interface*
function getUserById(id: number):UserModel | undefined {
return users.find((user)=>user.id === id)
}
console.log(getUserById(2));
````
>resultat 
>
>on a bien l'user avec l'id N° 2
>
>id 2; lastName: 'toto1', age:35,

note :On ajoute **| indefined** si user id n'existe pas .
