# [Les verbes des requêtes]()

 * [ Get](#Get)
 
 * [Delete](#Delete)
 
 * [Post](#Post)
 
 * [Put](#Put)
 
 * [Patch](#Patch)
 
 * [Qu'est ce qu'une API](#Qu'est ce qu'une API)
 
 >à savoir les navigateurs internet n'utilisent que la requête **get** 
>pour utiliser les autres verbes il faut télécharger 
>un logiciel qui s'appelle PostMan


## get:

leverbe get sert à faire une requête au server
et pour fair cela on écrit

````javascript
app.get('/api', (req, res) => {
    return res.send('Hello world!');
});
```` 
ie: 
**je fais une requête sur le server (app.get)avec pour URL'/api', jai en paramètre 
ma requête (req)et ma réponse (res)qui me retourn (return)un envoi du server de
 la réponse à ma requête (res.send)**
 
On peut aussi faire une raquête avec des objets 

Exemple 

````javascript
app.get('/api/user', (req, res) => {
    return res.send(user);
});
````
A noter : On ne peut pas appeler un nombre or si on veut appeler *user.age* par exemple,
il faut le mettre dans un vouvel objet comme suite 


````javascript
app.get('/api/user/age', (req, res) => {
    return res.send({age: user.age});
});
````
## Delete:

> ce verbe sert à supprimer un élément de la base de données

si on prend la liste des users on lui dit 
je voudrai suprimer de la liste des users **id n°2**

et dans l'URL de postMan on ecrit <http://localhost 3015/api/users 2>
ie tu va supprimer dans la base de donnée les personnes que je t'indique 
dans ce cas c'est *user 2*
exemple

````javascript
app.delete('/api/users/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    console.log(id, typeof id);
    users = users.filter((user) => user.id !== id);
    return res.send(users);
});
````
>note **params ,permet de recupérer les élements dans la variable dans ce cas c'est id 2
>**parsInt** permet de trensformer les strings en number

cela veux dir que :
je fais une requête pour supprimer dans le server (app.delete)avec 
pour l'URL'/api/users/:id,j'ai en parmaètre ma requête (req)et ma reponse (res)
qui me retourne (return)un envoi du server de la reponse à ma requete (req.send) et qui sert à supprimer 
les users parid,on renvois tout les user qui ont un id
diffirent de la variable id (users = users.filter((user)=>user.id !===id)) 

## Post:

pste permet de rajouter un élement de la base de donnéés( à une liste de tableau)

* ie:on va envoyer un nouvel utilisateur(objet) et on fait **Push**
* json est un format pour envoyer des objets javascript
dans l'aplication *PostMan* aller dans body ->text->json

````javascript
{
  "lastname": "toto1",
  "age": 35,
  "car": true
}
```` 
>à noter dans json ilfaut mettre des doubles guillemets (" " )
>pour les strings mais aussi pour les attributs.il faut aussi enlever
>la virgule à la fin d'un objet .On ne peut mettre que des strings,des numbers et booleans

dans l'IDE on écrit:

````javascript
app.post('/api/users, async (req, res) => {
    const newUser = req.body;
//on rajoute un id
    const id = users.length + 1;
//on push et on et on distructure (newUser)
    users.push({...newUser, id});
    return res.send(users);
});
````
> pour utiliser le req.body il faut importer bodyPrser comme tel:

````javascript
import bodyParser from 'bodyParser';

app.use(bodyParser({}));
````

## Put:
put est un peu particulier il va prendre un objet dans la console et il le
remplace par l'objet qu'on lui donne 

*Dans l'plication PostMan , ol faut séléctioner le verbe Put,entrer l'URL
des users avec id de user qu'on veux modifier puis 

exemple:
````javascript
{
  "lastname": "nouveau mec",
  "age": 102,
  "car": true
}
````
* Si on veux modifier ,on a plusieurs maniere de le fairs
soit avec la boucle for,un forEach,un map mais le mieux est d'utiliser **find index**

````javascript
app.put('/api/users/:id, async (req, res) => {
    const newUser = req.body;
    const id = parseInt(req.params.id);
    const index = users.findIndex((user) => user.id === id);
    users[index] = {...newUser, id}
    return res.send(users);
});
````
> pour une grosse base de donnée ne jamais faire un return de res .send(users)
>mais faire un return de res.send({result:'ok'})

## Patch:

>il set à modifier un ou plusieurs attributs d'un objet(user) de la base de donnée
Dans la console body PostMan on garde que ce q'on veux changer dans ce cas 
>on veux changer "lastName":"newMec",

````javascript
{
  "lastname": "nouveau mec",

}
````
* Dans l'IDE on fait la recherche par index de l'objet qu'on veux mmodifier
un ou plusieurs attributs

````javascript
app.put('/api/users/:id, async (req, res) => {
    const newUser = req.body;
    const id = parseInt(req.params.id);
//ie trouve moi user.id et tu me le remplace par le vrais id que je te donne 
    const index = users.findIndex((user) => user.id === id);
//on prend la nouvel valeur de user .indexsoit = à l'encienne valeur
//distructurer puis je rajoute le nouveau élément distructurer...newUser
    users[index] = {...users[index], ...newUser, id}
    return res.send(users);
});
````
## Qu'est ce qu'une API:

        Une API est un contrat entere deux ordinateurs pour savoir comment changer des 
        informations et comment utiliser les verbes et les protocoles
        ie comment on utilise app.get     
                              app.Delete     
                              app.Post  
                              app.Patch    
                              app.Put   