# [à quoi sert try catch](#Qu'est ce quetry catch)

* [comment utiliser try et catch]()
-----------------------------------------------------


## A quoi sert try catch:

>try catch => en français :attrape des erreures 
 
On utilise Try catch, car si on a des codes qui sont mal ércit
le server qui héberge nos applications va les cracher 
dès que le server crache il faut le redemarrer

## ce qu'il faut faire pour éviter que le server tourne en boucle :
 
* On englobe les opérations qu'il fait dans un  try catch celà permet 
de courriger afin de ne pas rendre le systhème déffectueux

* Il faut fair de try catch pour toutes les routes 

  * get 
  * put
  * patch
  * post 
  * delete

## comment utiliser try et catch:

* exemple:

````javascript
app.get('/api/hostels', (req, res) => {
try {
    return res.send('hello world 2');
} catch (e){
     console.error(e.message);
}   return res
    .status(500)
    .send({error: 'error serveur:' + e.message});

});
````

* .status( 500)=> que le serveur à planté
* Dans une fonction le **try fait appelle à une fonction** 
* et **catch il returne l'erreur**