# [les fonctions]()

* [ Qu'est ce que c'est qu'une fonction]()

  + [Fonction sans les paramètre]()
  + [Fonction avec les paramètres]()

* [les arrow function](#les arrow functionles arrow function)

* [rest Parameter]()
    + [Addition avec (...args)]() 
    
    + [fair une function string au debut et addition à la fin]()

 >Dans le server node ce qu'est va changer c'est que les resultats vont être afficher 
dans la console node contrairement à avant elle s'afichait dans la console pricipal

## [Qu'est ce que c'est qu'une fonction]():

est une machine qui prend en  entrer des information et qui 
fabrique une sortie

exemple:

                  une machine
                   | 
[ une entrer -----> ![               ](images) ---->une sortie]()
 
> c'est tout comme map reduce sauf que cest dernieres sont utiliser par javascript

## [Fonction sans les paramètre]():

 il est possible qu'une entrer il n'ya riens ie onaura aucun paramètre 
 une fonction se déclare comme suite :
 ````javascript
//on déclare une fonction sans mettre des paramèttres à l'interieur 
function myFunction(){
// on va lui fair fair qu'elque chose par exemple
console.log('coucou');
}

//la on va appeler la fonction et pour appeller une fonction on met des parenthèses àla fin
myFunction();
````
 resulta
 on voit **coucou** qui s'afiche dans la console de node si on avait appeler 
 la fonction plusieurs fois on aura autant de **coucou**qui safiche
 
## [ Fonction avec les paramètres]()

 on donne les paramètres qu'on veux exo:a et b
 qu'on peux les manipuler comme si on a une constent (a,b)
 
 ````javascript
function myFunction(a,b){
console.log(a + b);

}
//si on met rien dans myFunction 
myFunction()
````
au resultat on aura **NAN** **a** nous donne indifined
et **b** nous donne indefined donc indifined + indefined
nous donne **not a number**
 
 
 ````javascript
function myFunction(a,b){
console.log(a + b);

}
//si on met par exemple a = 2et b = 3 dans myFunction 
myFunction(2 +3)
````
   au resultat on aura 
   l'addition de 2 et de 3 
   5
> à noter jusqu'a mintenant la fonction ne fait pas de sortie 
> a des jents exteriuer car elle fait que de console loger

je prend par exemple une fonction d'addition 

````javascript
function add(a, b){
return (a + b);
}
console.log(add(2 + 3));
````
>resultat est: 5

vu que cela reurne quelque chose on difinir une constente
à l'exterieur de la function et dir que constc'est le resultat
de add(2et 3) donc si on fait un console.log de result on aura 5 à la console

exemple:

>note : quant on met une function la **const result = add(2,3)
>on peut l'a déplacer avant ouaprés la function cela nous donne 
>toujours le même resultat

````javascript
function add(a, b){
return (a + b);

}
const result = add(2,3)

console.log(result);
````
au resultat onaura 5 pour quoi ?
car on prend a et b on les additionnent et on les retourne
**les (2,3) qu'ona dans const vont être stocker grace à l'insignation (=)
dans result** 

## [les arrow function]()

C'est une autre fçon de déclarer une fonction

on va crée une constente **addBis** = a une arrow function 
qui va prendre a et b en paramètre et elle retourne (a + b)

exemple:

````javascript
const addBis = (a, b)=>a + b

const resultBis = addBis(2, 10)

console.log(resultBis);
````
 >au resultat on aura bien 
>12

>On note que les deux fonctions sot pareille ,juste pour arrow function 
>
>on ne peut pas la deplacer ,ie en javascript on ne peut pas mettre 
>
>le resultat avant d'avoir déclarer l'arrow function car on met une arrow function à l'interieur de la constente

````javascript
const addBis = (a, b)=>a + b
//est une abriviation de :
const addBis = (a, b)=>{
return a + b;
}
````
## [rest Parameter]():

> est un opérateur interessant avec les fonction car elle permet 
 d'avoir les fonctions sans préciser tout les arguments .

Exemple 

si on veut une function qui aura plusieurs argiment 

**on utilise un outil trés pratique (...args)**

````javascript
function add(...args){
console.log(args);
return args;
}
add(2, 3, 4, 5)
````
au resultat il nous renvoit un tableau
>[[2, 3, 4, 5]]()

* [Addition avec (...args)]()  

  Comme args est array de type string si on veut faire l'addition de tout 
  mes array string on doit fair returnde **raduce**afin de faire une concatination
  
 voir l'exemple suivant:
 
 ````javascript
function add(...args){
console.log(args);
return args.reduce((acc, value)=>acc + value, 0 );
}
console.log(add(2, 3, 4, 5));
````   
resultat 
>[2, 3, 4, 5]
>
>14

* [fair une function string au debut et addition à la fin]()

````javascript
function add(message,tata,toto, ...args){
const result = args.reduce((acc,value)=>acc + value, 0)
console.log(message, result);
}
add('le resultat est:'2, 3, 4, 5);
````
note :
----
>function add(message,tata,toto, ...args)
le reste paraméter doit être le dernier de la list cela s'appelle rest
 car c'est le dernier argumet de la list
quand on sais qu'on va avoir plusiers paramètre et qu'on les connait pas 
c'est ceux qui rest des paramètres.c'est pour cela **...args**s'appelle 
**le reste paraméter**
cette outil est trés interéssant quand on à une function qui doit manipuler 
des tableaux ou bien autre choses on utilise le rest Opérator