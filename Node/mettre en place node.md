# [Mettre en place node](#Mettre en place node)

* [Installer node](#Installer node)

* [Exeécuter plusieurs taches simultanément](#Exeécuter plusieurs taches simultanément)

* [Créer et importer l'application express](#Créer et importer l'application express)

*[ faire une requete au serveur](#faire une requete au serveur)

* [Ecouter le serveur](#Ecouter le serveur)



## Installer node:

   * Pour installer node ilfaut aller dans le dossier server
   
   avant de lancer la commande d'instalation **npm i**, il faut bien penser à changer  le dossier en dossier 
   server en écrivant dans terminal **cd server** une fois installer 
   on écrit  **npm i** clic aussi sur entrer pour qu'il installe
   
   il faut ensuite installer les élléments configurer dans le fichier 
   Package.json avecla commande **npm install**
   
##  Exeécuter plusieurs taches simultanément:

Pour exécuter plusieurs taches simultanément, il faut installer **npm concrrently
pour cela il faut lancer la commande **npm install -g concurrently**

-g ie global.
puis avant tout il faut lancer watch ensuit lancer server 

pour aficher le script du server clic droit sur packag.json ->Show npm scripts


## Créer et importer l'application express:

Expresse est un otil donnée par node pour lancer un serveur ie c'est un server de node

>pour créer et importer l'application express il faut coder :

````javascript
import express from 'express';

const app = express();
````

## faire une requete au serveur:

pour faire une requête, on utilise l'application express en appelant sa variable
 
 dans ce cas leverbe il determine une action pour fair une requête on 
 utilise GET
 

````
app.get('/api', (req, res) => {
    return res.send('Hello world!');
});
``````
Cela signifie:

je fais une requête sur le server (app.get) avec pour URL'/api,j'ai en paramètre ma requête
(req)et maréponse(res) qui me retourne(return) un envoi du server de la réponse à ma requête(res.send)

## Ecouter le serveur:
 > Pour ecouter le server, on utilise l'application expressen appelant sa variable

leverbe pour écouter le server c'est **listen**

exemple 

````javascript
app.listen(3015, function() {
    console.log('API listening on http://localhost:3015/api/ !');
});
````
ie: j'écoute le server (app.listen),avec en paramètre le **port**(3015)
et une fonction (function()) qui permet d'écrir dans la console du server
j'écoute sur lURL <http://localhost:3015/api/> cela permet à l'utilisateur 
d'avoir des information sur ce qui ce passe dans le server
à savoir dans le server on code en typeScript
