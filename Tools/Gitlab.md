# [gitlab](#gitlab)

* [installer Git sur Webstorm](#installer Git sur Webstorm)

* [configurer ses parameters](#configurer ses parameters)

* [ignorer des fichiers avec gitignore](#ignorer des fichiers avec gitignore])

* [supprimer des fichiers déjà pusher avec gitignore](#supprimer des fchiers gitignore)

* [comite un fichier sur Gitlab](#comite un fichier sur Gitlab)

* [récupérer un fichier de Gitlab](#récupérer un fichier de Gitlab)

* [Utilisr les branches](#Utilisr les branches)

* [travailler à plusieurs](#travailler a plusiuers)
-----------------------------------------------------------------------------------------

> Note d'information: à savior que Git est protocole
>et Gitlab est un site internet ,c'estun outil qui exploite le protocole Gite
>ainsi que github, bitbucket... .

 **avant tout il faut crée un fichier (repository) dit: repo, appeler Readme.md**
 
 ## installer Git sur Webstorm:
 
 * ouvrir le terminal et taper les instructions:    *git init*
 
 ## configurer ses parameters:
 
 * ouvrir la console dans Webstorm 
 * Allez dans son navigateur et chercher **git set username and password** 
 * Allez sur le site `atlassian`
 * copier coller dans la console les instructions du paragraphe *Configure your Git username /email*
 * Allez sur Webstorm => File =>sittings
 * Activer le plugin Gitlab,dans le market place  chercher Gitlab project ,installer le et redemarrer Webstorm
 * réouvrir les sittings et cliquer sur version control=>Gitlab
 * Add a new server 
 * mettre !http://gitlab.com, gitlab.com puis cliquer sur token page
 * crée une clé API avec le personel token pour Webstorm
 
 ## ignorer des fichiers avec gitignore:
 
* pour ne pas envoyer des fichiers inutiles sur gitlab (cela n'est pas eco responsable et ne sert à rien)
 
* Créer un fichier gitignore 
* entrer les noms des fichiers et dossiers qu'on souhaite ignorer 
comme: _idea, node-modules, firbaserc, mesdata secretes.js_

## supprimer des fichiers déjà pusher avec gitignore:

* si on a pusher des fichiers ou des dossiers avant de crée le fichier gitignore il faut le remove:
   
        git rm -rf nomDuFichier 
     
> cela peut parfois faire bugger Webstorm, il suffit de relancer l'IDE

## comite un fichier sur Gitlab:

* le protocole Git est lancé mais le fichier n'est pas encore partagé sur Git (fichier en rouge)
* fair clic droit sur le fichier REPO =>GIT => add(le fichire passera au vert)
* **faire un commit:** pour envoiyer le fichier sur Git il faut cliquer sur la flèche verte 
* une fenetre decomit s'ouvre il faut écrir un méssage pur signalre 
 l'action que l'on fait (add new file readme)
 
 * cliquer sur commit and push
 * entrer ses identifiants  Gitlab si ce n'est pas déjà fait
 *selectionner le projet souhaiter dans Gitlab, pour qu'on crée un nouveau projet, soit selectionner un projet 
 existant cliquer sur clone et coupier coller l'URL dans la fenêtre de webstorm
 * cliquer sur push
 * le fichier passe en blanc,ie il est bien ajouter  dans le projet en cours sur Gitlab
 
## récupérer un fichier de Gitlab:

* Allez dans son projet  sur Gitlab
* sélectionner le fichier à recuérer
* cliquer sur clone coupier l'URL dans Webstorm
 * Allez dans VCS =>Get version control
 * coller l'URL dans la fênetre ouvert
 * cliquer sur cloner
 * le fichier est uploader dans Webstorm
 
## Utilisr les branches:

* Cliquer sur Git master 
* selectionner new branche  et le nommer (exo: Farida)
* commit and push la nouvelle branche ,cela permet de mmodifier le fichier sans toucher au
 fichier master qui est le fichier de réferance
 * il faut fair un chek out pour passer d'une branche à une autre 
 **si modification toujours les fair sur sa branche et jamais dans la master**
 une fois le travail fini onfait on fait un **merge request**
 pour demander au chef de projet d'integrerles modifications de master dans Gitlab
 * si les modifications sont accepter (le chef de projet merge et delete la branche secondaire)
 * on doit récupérer le fichier master en faisant un check out sur le master
 * on pull avec la flèche bleu pour mettre à jour le nouveau fichier master
 
## travailler à plusieurs:

>Attention il ne faut pas faire de merge request si on est pas sûsr de son resultat!

* une branche doit s'appeller par feature (une fonctionnalité)
* lorqu'un conflit se trouve  dans les merges , 
ne pas resoudre directement le conflit dans Gitlab
* Envoyer un message de mise à jour à la personne consernée
* cett personne est en charge de régler le conflit
* si désaccord voir le chef de projet 
* on nepeux plus commiter car un merge n'est pas reconnu comme une modification 
* faire Shift + ctr + k pour fair un push 
* le chef de projet reçoit une notification et peux vérifier si les conflits sont églés *
 le master est de nouveau propre 